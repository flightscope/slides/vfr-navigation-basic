#!/usr/bin/env bash

set -e
set -x

if ! type -p rsync >/dev/null ; then
  >&2 echo "Missing: rsync" >&2
fi

if ! type -p libreoffice >/dev/null ; then
  >&2 echo "Missing: libreoffice" >&2
fi

if ! type -p qpdf >/dev/null ; then
  >&2 echo "Missing: qpdf" >&2
fi

if ! type -p pdftk >/dev/null ; then
  >&2 echo "Missing: pdftk" >&2
fi

if ! type -p pdfjam >/dev/null ; then
  >&2 echo "Missing: pdfjam" >&2
fi

script_dir=$(dirname "$0")
dist_dir="${script_dir}/../public"
etc_dir="${script_dir}/../etc"
handout_dir="${dist_dir}/handout"
mkdir -p "${handout_dir}"

ersa_date="24MAR2022"

copy() {
  rsync -aH "${etc_dir}/flight-notification-form-portrait.pdf" "${handout_dir}"
  rsync -aH "${etc_dir}/aip.pdf" -c -P "${handout_dir}"
  rsync -aH "${etc_dir}/fuel-plan-portrait.pdf" "${handout_dir}"
  rsync -aH "${etc_dir}/FAC_YBAF_${ersa_date}.pdf" "${handout_dir}"
  rsync -aH "${etc_dir}/FAC_YHEC_${ersa_date}.pdf" "${handout_dir}"
  rsync -aH "${etc_dir}/FAC_YBCG_${ersa_date}.pdf" "${handout_dir}"
}

cover() {
  libreoffice --invisible --headless --convert-to pdf "${etc_dir}/cover.fodt" --outdir "${handout_dir}"
}

decrypt() {
  qpdf \
    --decrypt \
    ${handout_dir}/FAC_YBAF_${ersa_date}.pdf \
    ${handout_dir}/FAC_YBAF_${ersa_date}-decrypted.pdf

  qpdf \
    --decrypt \
    ${handout_dir}/FAC_YHEC_${ersa_date}.pdf \
    ${handout_dir}/FAC_YHEC_${ersa_date}-decrypted.pdf

  qpdf \
    --decrypt \
    ${handout_dir}/FAC_YBCG_${ersa_date}.pdf \
    ${handout_dir}/FAC_YBCG_${ersa_date}-decrypted.pdf
}

fuelplan_page1() {
  pdftk \
    "${handout_dir}/fuel-plan-portrait.pdf" \
    cat 1 output \
    "${handout_dir}/fuel-plan-portrait-page1.pdf"

}

joinersa() {
  pdftk \
    "${handout_dir}/FAC_YBAF_${ersa_date}-decrypted.pdf" \
    "${handout_dir}/FAC_YHEC_${ersa_date}-decrypted.pdf" \
    "${handout_dir}/FAC_YBCG_${ersa_date}-decrypted.pdf" \
    cat output \
    "${handout_dir}/ersa.pdf"
}

pagesplit() {
  pdfjam --landscape --nup 2x1 "${handout_dir}/ersa.pdf" --outfile "${handout_dir}/ersa-split.pdf"
}

joinpdf() {
  pdftk \
    "${handout_dir}/cover.pdf" \
    "${handout_dir}/flight-notification-form-portrait.pdf" \
    "${handout_dir}/flight-notification-form-portrait.pdf" \
    "${handout_dir}/flight-notification-form-portrait.pdf" \
    "${handout_dir}/fuel-plan-portrait.pdf" \
    "${handout_dir}/fuel-plan-portrait-page1.pdf" \
    "${handout_dir}/fuel-plan-portrait-page1.pdf" \
    "${handout_dir}/ersa-split.pdf" \
    cat output \
    "${handout_dir}/handout.pdf"
}

copy
cover
decrypt
joinersa
fuelplan_page1
pagesplit
joinpdf
