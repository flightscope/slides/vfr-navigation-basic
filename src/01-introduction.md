## Basic VFR Navigation { data-transition="convex-in" }

###### Materials

* You will need
  * Pen/Pencil/Eraser/Sharpener
  * Ruler
  * Basic Calculator _(or phone calculator)_
* Provided
  * Brisbane Visual Terminal Chart (VTC)
  * Basic navigation equipment
  * Navigation Log
  * ERSA FAC entries
  * <a href="handout/handout.pdf">Link to handout</a>

---

## Basic VFR Navigation

###### Aims and Objectives

* To learn some basic concepts that apply to navigation training that you can practice at home and/or in-flight
  * dead reckoning
  * visual navigation procedures and workflows
  * magnetic variation
  * fuel management
  * applicable air legislation
* To plan a simple navigation flight *(on paper)* so that you can then later practice similar
* Discuss how your first navigation training flight might look

---

## Basic VFR Navigation

###### Applicability

* You will be examined on visual navigation:
  * RPC navigation endorsement
  * RPL navigation endorsement
  * PPL
  * CPL
* Although it is often less procedural, we are visually navigating when training:
  * RPC training
  * RPL/PPL/CPL training
  * Other Endorsement/Rating training
  * Your instructor will typically be managing navigation so you can focus on specific training objectives
  * However, don't let that stop you from contributing to visual navigation!

---

## AIP ENR 1.1 { data-transition="concave-out" }

###### 4. NAVIGATION REQUIREMENTS

<p style="text-align: center">
  <a href="images/vfr-navigation-30-minutes.png">
    <img src="images/vfr-navigation-30-minutes.png" alt="VFR Navigation 30 minutes" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

