## Our mission { data-transition="convex-in" }

<p style="text-align: center">
  <img src="images/nav-mission.png" alt="Navigation Mission" width="750px" style="border: 4px solid #555"/>
</p>

---

## Navigation Log

<p style="text-align: center; font-size: x-large">
  Write your three positions (PSN) on your navigation log
</p>

<p style="text-align: center">
  <a href="images/vfr-nav-basic-1.png">
    <img src="images/vfr-nav-basic-1.png" alt="Navigation Mission 1" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Visual Terminal Chart YBAF &rarr; YHEC &rarr; YBCG

<p style="text-align: center">
  <a href="images/vfr-nav-basic-2.png">
    <img src="images/vfr-nav-basic-2.png" alt="Navigation Mission 2" width="480px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Visual Terminal Chart YBAF &rarr; YHEC &rarr; YBCG

<p style="text-align: center">
  <a href="images/vfr-nav-basic-3.png">
    <img src="images/vfr-nav-basic-3.png" alt="Navigation Mission 3" width="480px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Using your protractor

<p style="text-align: center; font-size: x-large">
  Determine your two Flight Plan Tracks (FPT)
</p>

<p style="text-align: center">
  <a href="images/vfr-nav-basic-4.png">
    <img src="images/vfr-nav-basic-4.png" alt="Navigation Mission 4" width="440px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Take a side note of our two FPT { data-transition="concave-out" }

| Leg      | FPT |
| -------- | --- |
| YBAF &rarr; YHEC | 124 |
| YHEC &rarr; YBCG | 160 |

---

