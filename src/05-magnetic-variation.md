## Before we go any further &hellip; { data-transition="convex-in" }

<p style="text-align: center; font-size: x-large">
  we need to talk about a little something called <b>Magnetic Variation</b>
</p>

---

## Every day, this happens

<p style="text-align: center">
  <a href="images/earth-rotating.gif">
    <img src="images/earth-rotating.gif" alt="Earth Rotating" width="440px" style="border: 4px solid #555"/>
  </a>
</p>

---

## North

* The Earth spins on an axis each day
* The northern point of that axis is called the **Geographic North Pole**
* Also sometimes called **True North**, abbreviated **T**

<p style="text-align: center">
  <a href="images/earth-rotating.gif">
    <img src="images/earth-rotating.gif" alt="Earth Rotating" width="240px" style="border: 4px solid #555"/>
  </a>
</p>

---

## North

* The North on our aeronautical charts aligns with **True North**
* Therefore, the Flight Plan Tracks we just calculated are with reference to True North

<p style="text-align: center">
  <a href="images/earth-rotating.gif">
    <img src="images/earth-rotating.gif" alt="Earth Rotating" width="240px" style="border: 4px solid #555"/>
  </a>
</p>

---

## True North

| Leg      | FPT |
| -------- | --- |
| YBAF &rarr; YHEC | 124<span style="color: red">T</span> |
| YHEC &rarr; YBCG | 160<span style="color: red">T</span> |

---

## Aircraft Instruments

<p style="text-align: center; font-size: x-large">
  But what about the navigation instruments in our aircraft?
</p>

<p style="text-align: center">
  <a href="images/8881-cockpit-nav-instruments.jpg">
    <img src="images/8881-cockpit-nav-instruments.jpg" alt="8881 Cockpit" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Aircraft Instruments

* Aircraft navigation instruments are aligned using a <span style="color:red; font-weight: bold">magnet</span>
* So which way does a magnet point?
* Ever tried it?

<p style="text-align: center">
  <a href="images/magnet-experiment.png">
    <img src="images/magnet-experiment.png" alt="Magnet experiment" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Aircraft Instruments

* A magnet aligns with Earth's <span style="color:red; font-weight: bold">magnetic field</span>
* Therefore, so do our aircraft navigation instruments

<p style="text-align: center">
  <a href="images/earth-magnetic-field.png">
    <img src="images/earth-magnetic-field.png" alt="Earth Magnetic Field" width="620px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Aircraft Instruments

* The "north" direction a magnet points is called **Magnetic North**
* The *actual* direction of Magnetic North **depends on where you are on Earth**
* The difference between True North and Magnetic North, for a given location, is called &hellip;
* <span style="color:red; font-weight: bold">Magnetic Variation</span>
* *Note: also sometimes called Magnetic Declination &mdash; same same*

---

## Magnetic Variation

* We need to convert these Flight Plan Tracks with reference to Magnetic North for our location
* Where might we find the magnetic variation for a location?

| Leg      | FPT |
| -------- | --- |
| YBAF &rarr; YHEC | 124<span style="color: red">T</span> |
| YHEC &rarr; YBCG | 160<span style="color: red">T</span> |

---

## Magnetic Variation

<p style="text-align: center">
  <a href="images/vnc-isogonal.png">
    <img src="images/vnc-isogonal.png" alt="VNC Isogonal" width="300px" style="border: 4px solid #555"/>
  </a>
</p>

* Not on the VTC
* But you will find lines of magnetic variation on the Visual Navigation Chart (VNC)
* These lines are called <span style="color:red; font-weight: bold">isogonals</span>
* The magnetic variation on this isogonal is 11 degrees East

<p style="text-align: center">
  <a href="images/vnc-isogonal-2.png">
    <img src="images/vnc-isogonal-2.png" alt="VNC Isogonal" width="200px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Magnetic Variation

* You will also find Magnetic Variation for a location in the ERSA
* In fact, the Magnetic Variation is <span style="color: red; font-weight: bold">11&deg; E</span> for our entire flight plan
* Look it up!

<p style="text-align: center">
  <a href="images/ybaf-magvar.png">
    <img src="images/ybaf-magvar.png" alt="YBAF Magnetic Variation" width="800px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Magnetic Variation { data-transition="concave-out" }

* Since the variation is East, we **subtract** from True North
* *Common Error: converting the wrong way*

| Leg      | FPT | FPT |
| -------- | --- | --- |
| YBAF &rarr; YHEC | 124<span style="color: red">T</span> | 113<span style="color: red">M</span>
| YHEC &rarr; YBCG | 160<span style="color: red">T</span> | 149<span style="color: red">M</span>

---

