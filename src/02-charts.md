## Aeronautical Charts { data-transition="convex-in" }

* AirServices publishes various approved aeronautical charts for navigation
  * Visual Terminal Chart (VTC)
  * Visual Navigation Chart (VNC)
  * World Aeronautical Chart (WAC)
  * En-Route Chart (ERC)
  * Planning Chart Australia (PCA)

---

## Aeronautical Charts

* For our purposes in this briefing, we will be using the Visual Terminal Chart (VTC)
  * VTC scale: `1:250000`
  * Updated every 6 months
  * Contains many detailed ground features
  * Designed for visual operations near terminal areas (e.g. YBBN, YBCG)
  * Can be purchased for approximately $14 or freely available online
  * There is one in your training aircraft!

---

## Brisbane/Gold Coast Visual Terminal Chart

<p style="text-align: center">
  <a href="images/vtc.png">
    <img src="images/vtc.png" alt="Visual Terminal Chart" width="1000px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Brisbane/Gold Coast Visual Terminal Chart { data-transition="slide concave-out" }

<p style="text-align: center">
  <a href="images/vtc-annotated.png">
    <img src="images/vtc-annotated.png" alt="Visual Terminal Chart annotated" width="1000px" style="border: 4px solid #555"/>
  </a>
</p>
