## Happy Flying! { data-transition="convex-in" }

<p style="text-align: center">
  <a href="images/8881-day1.jpg">
    <img src="images/8881-day1.jpg" alt="8881 Day 1" width="650px" style="border: 4px solid #555"/>
  </a>
</p>

---

<div style="border: 4px solid #555">
  <p style="text-align: center; font-size: x-large;">
    A girl goes walking alone, with a magnetic compass.
  </p>
  <p style="text-align: center; font-size: x-large;">
    She encounters a bear, so walks one nautical mile south, one nautical mile east, one nautical mile north.
  </p>
  <p style="text-align: center; font-size: x-large;">
    She finds she is back at where she started.
  </p>
  <p style="text-align: center; font-size: x-large;">
    What colour is the bear?
  </p>
</div>
