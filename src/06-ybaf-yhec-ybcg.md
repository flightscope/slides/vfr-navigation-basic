## Navigation Log { data-transition="convex-in" }

* Back to our navigation log
* Write your **magnetic tracks** for your planned flight

<p style="text-align: center">
  <a href="images/vfr-nav-basic-5.png">
    <img src="images/vfr-nav-basic-5.png" alt="Navigation Mission 5" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Distance

* Using your navigation ruler, measure the distance for each leg of our flight
* Be careful to use the correct scale for the VTC: **1:250000**

<p style="text-align: center">
  <a href="images/nav-ruler.png">
    <img src="images/nav-ruler.png" alt="Navigation Ruler" width="780px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log

<p style="text-align: center; font-size: x-large">
  Enter the distances into our Navigation Log
</p>

<p style="text-align: center">
  <a href="images/vfr-nav-basic-6.png">
    <img src="images/vfr-nav-basic-6.png" alt="Navigation Mission 6" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log

* TAS = **True Air Speed**
* TAS is a function of Indicated Air Speed (IAS) and **Density Altitude**
* Density Altitude is a function of **Pressure Altitude** and **Outside Air Temperature**
* Pressure Altitude is a function of **QNH** and **Elevation**
* That's a lot of functions!
* There are practice drills for calculating Density Altitude (DA) under Student Resources on our website!
* You can also calculate TAS and DA on a Flight Computer (e.g. E6B)

<p style="text-align: center">
  <a href="images/e6b-da.png">
    <img src="images/e6b-da.png" alt="E6B Density Altitude" width="450px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log

* Or you could just cheat with modern avionics
* Ever noticed?

<p style="text-align: center">
  <a href="images/8881-da-tas.png">
    <img src="images/8881-da-tas.png" alt="8881 Density Altitude, TAS" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log

* For the purposes of our mission, TAS is constant
* **90KTAS**
* This is denoted: <span style="color: red; font-weight: bold">N0090</span>

---

## Navigation Log

<p style="text-align: center; font-size: x-large">
  Enter the TAS into our Navigation Log
</p>

<p style="text-align: center">
  <a href="images/vfr-nav-basic-7.png">
    <img src="images/vfr-nav-basic-7.png" alt="Navigation Mission 7" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log

* Altitude considerations
  * Terrain
  * Controlled Airspace
  * Weather
  * VFR Cruising Altitudes
  * Aircraft performance
* For our mission, the selected altitude is 1500ft
* This is denoted <span style="color: red; font-weight: bold">A015</span>

---

## Navigation Log

<p style="text-align: center; font-size: x-large">
  Enter the Altitude into our Navigation Log
</p>

<p style="text-align: center">
  <a href="images/vfr-nav-basic-8.png">
    <img src="images/vfr-nav-basic-8.png" alt="Navigation Mission 8" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log

* We log in to AirServices NAIPS to obtaining a complete weather briefing
* The briefing contains the Grid Point Wind Forecast (GPWT) for the time of our flight
* The wind is forecast at 1500ft to be 210 at 23 knots
* However, the GPWT wind is with reference to **True North**
* At 11&deg;E magvar, we find the wind to be *(rounded)*&hellip;
* <span style="color: red; font-weight: bold">200M</span> at <span style="color: red; font-weight: bold">23 knots</span>

---

## Navigation Log

<p style="text-align: center; font-size: x-large">
  Enter the forecast wind into our Navigation Log
</p>

<p style="text-align: center">
  <a href="images/vfr-nav-basic-9.png">
    <img src="images/vfr-nav-basic-9.png" alt="Navigation Mission 9" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log

* There is a difference between **Track** and **Heading**
* Track is the flight path over the ground that the aeroplane makes &mdash; we have a Flight Plan **Track**
* Heading is the direction that the aeroplane is pointing
* We have a **Heading Indicator** instrument in our aircraft
* Heading is affected by the crosswind
* The angular difference between Heading and Track is called **Drift**
* We need a **Wind Correction Angle** to compensate for drift
* Furthermore, **Ground Speed** is affected by the head/tail wind

---

## Navigation Log

<p style="text-align: center; font-size: x-large">
  The difference between Heading and Track
</p>

<p style="text-align: center">
  <a href="images/drift.png">
    <img src="images/drift.png" alt="Drift" width="350px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log

<p style="text-align: center; font-size: x-large">
  We need to calculate our <b>Heading</b> and <b>Ground Speed</b>
</p>

---

## Navigation Log

* We will use the E6B FLIGHT COMPUTER to calculate Heading and Ground Speed
* Other brands of flight computer may be slightly different in the process, but certainly similar

<p style="text-align: center">
  <a href="images/e6b.jpg">
    <img src="images/e6b.jpg" alt="E6B" width="260px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log { data-transition="zoom-in" }

* We have determined the wind to be <code>200</code> at <code>23</code> knots
* Turn and set <code>200</code> under **True Index** on the E6B

<p style="text-align: center">
  <a href="images/e6b-1.jpg">
    <img src="images/e6b-1.jpg" alt="E6B 1" width="360px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log { data-transition="zoom-in" }

* We know our TAS to be <code>90</code> knots
* Slide the E6B so that <code>90</code> appears under the centre mark

<p style="text-align: center">
  <a href="images/e6b-1.jpg">
    <img src="images/e6b-1.jpg" alt="E6B 1" width="360px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log { data-transition="zoom-in" }

* We know the wind to be <code>23</code> knots
* Make a mark <code>23</code> above the centre mark of the E6B (i.e. the mark will be at <code>113</code>)

<p style="text-align: center">
  <a href="images/e6b-2x.png">
    <img src="images/e6b-2x.png" alt="E6B 2" width="360px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log { data-transition="zoom-in" }

* We know our initial track to be <code>113</code>
* Turn and set <code>113</code> under **True Index** on the E6B
* Slide the E6B so that our mark intersects our TAS (<code>90</code> knots)

<p style="text-align: center">
  <a href="images/e6b-3.png">
    <img src="images/e6b-3.png" alt="E6B 3" width="320px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log { data-transition="zoom-in" }

* Our ground speed is under the centre mark, <code>85</code> knots
* Our Wind Correction Angle to compensate for drift is the distance from the centre to our mark
* **Right turn 14 degrees**
* Therefore, for our track <code>113</code>, we need a **Heading** of <code>127</code>

<p style="text-align: center">
  <a href="images/e6b-3.png">
    <img src="images/e6b-3.png" alt="E6B 3" width="320px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log { data-transition="zoom-in" }

<p style="text-align: center; font-size: x-large">
  Enter the Heading and Ground Speed for our initial leg
</p>

<p style="text-align: center">
  <a href="images/vfr-nav-basic-10.png">
    <img src="images/vfr-nav-basic-10.png" alt="Navigation Mission 10" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log { data-transition="zoom-in" }

* Repeat the same process for the second leg (Track <code>149</code>) to obtain a new heading and ground speed
* We can reuse the mark because the wind will be the same

<p style="text-align: center">
  <a href="images/e6b-4.png">
    <img src="images/e6b-4.png" alt="E6B 4" width="300px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log { data-transition="zoom-in" }

* Wind Correction Angle: Right <code>12</code> degrees for Track <code>149</code>
* Heading: **161**
* Ground Speed: **73** knots

<p style="text-align: center">
  <a href="images/e6b-4.png">
    <img src="images/e6b-4.png" alt="E6B 4" width="300px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log

<p style="text-align: center; font-size: x-large">
  Enter the Heading and Ground Speed for our second leg
</p>

<p style="text-align: center">
  <a href="images/vfr-nav-basic-11.png">
    <img src="images/vfr-nav-basic-11.png" alt="Navigation Mission 11" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log

* Nearly there!
* We need to determine our Estimate Time Interval (ETI) for each leg
* Quite simply, how long will each leg take *(round to nearest minute)*?

---

## Navigation Log

* YBAF&rarr;YHEC
  * Ground speed: <code>85</code> knots
  * Distance: <code>21</code> NM
  * 21 ÷ 85 x 60
  * **15 minutes**
* YHEC&rarr;YBCG
  * Ground speed: <code>73</code> knots
  * Distance: <code>25</code> NM
  * 25 ÷ 73 x 60
  * **21 minutes**
* Note: *The E6B Flight Computer can also perform this calculation among many others*

---

## Navigation Log

<p style="text-align: center; font-size: x-large">
  Enter our estimated time intervals
</p>

<p style="text-align: center">
  <a href="images/vfr-nav-basic-12.png">
    <img src="images/vfr-nav-basic-12.png" alt="Navigation Mission 12" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log

* Lastly, we enter our Estimated Elapsed Time (EET)
* This is just the sum of our ETI for each leg

<p style="text-align: center">
  <a href="images/vfr-nav-basic-13.png">
    <img src="images/vfr-nav-basic-13.png" alt="Navigation Mission 13" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Navigation Log

* There are some other fields on a navigation log
* **LSALT**: Lowest Safe Altitude &mdash; this is more typically used for night or instrument flight
* **PLN EST**: Planned Estimated Time &mdash; this is recorded in-flight
* **REV EST**: Revised Estimated Time &mdash; this is recorded in-flight
* **ATA/ATD**: Actual Time of Arrival/Departure &mdash; this is recorded in-flight

---

## Navigation Log { data-transition="concave-out" }

* Since you have planned your flight so well, you can now relax and enjoy your flight
* You are ready to adapt to any unexpected changes in the plan
* In fact, you are so prepared, you can also take pictures!

---

