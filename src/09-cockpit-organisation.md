## Cockpit organisation { data-transition="convex-in" }

<p style="text-align: center">
  <a href="images/man-covered-in-notes.jpg">
    <img src="images/man-covered-in-notes.jpg" alt="Man Covered in Notes" width="280px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Cockpit organisation

* A well-planned navigation flight will have implemented a **lot** of prior planning
* However, not all of it is essential at any given time
* Cockpit organisation is about having the appropriate information available to you at that moment

---

## Cockpit organisation

###### Aeronautical charts

* A complete VTC or VNC is a very large document
* However, you will not need to reference the entire thing during your flight
* Instead, we will **fold** our chart so that all the relevant information is easily available to us
* When folding, leave a 10NM margin outside your planned route
* However, if for some unforeseen reason you need more chart information, ensure that it is still possible to get to it

---

## Cockpit organisation

###### ERSA

* The ERSA is a giant book
* You won't need all of it, but bring it anyway!
* Summarise the relevant information in the ERSA for your planned navigation and make it easily available
* If for some unforeseen reason in-flight, you decide you are diverting to Norfolk Island, well... the ERSA is still there

---

## Cockpit organisation { data-transition="concave-out" }

###### In short

* For a given flight or flight leg, make any relevant information easily and quickly available to you
* Any information that you **might** need, keep that out of the way, but still accessible
* Any information that you are **unlikely** to need, put it on the back seat or baggage area &mdash; you can still get to it if you need, but probably won't

---

