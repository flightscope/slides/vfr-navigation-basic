## Destination Facilities { data-transition="convex-in concave-out" }

* On any planned navigation, it is important to consider the facilities available at the other end
* Is there fuel available? What type of fuel?
* What type of fuel card is required to purchase fuel?
* If a fuel truck is available, is there an extra fee?
* Is the destination airport security controlled?
* Can I get back into the gate after I leave?
* Is there an internet connection to check weather, etc?
* Is it possible to tie the aircraft down overnight?
* If I do a daily inspection the next day and I need oil, where is it?

---

