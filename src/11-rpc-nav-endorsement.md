## RPC Navigation Endorsement { data-transition="convex-in" }

###### RA-Aus Operations Manual 2.01

<p style="text-align: center">
  <a href="ra-nav-endorsement-2.01.png">
    <img src="images/ra-nav-endorsement-2.01.png" alt="RA-Aus Operations Manual 2.01" width="880px" style="border: 4px solid #555"/>
  </a>
</p>

---

## RPC Navigation Endorsement

###### RA-Aus Operations Manual 2.07

<p style="text-align: center">
  <a href="images/ra-nav-endorsement-2.07.png">
    <img src="images/ra-nav-endorsement-2.07.png" alt="RA-Aus Operations Manual 2.07" width="880px" style="border: 4px solid #555"/>
  </a>
</p>

---

## RPC Navigation Endorsement

###### Tips

* Prior preparation is **key**
* Navigation training has additional weather criteria that may result in cancellation *(covered in training)*, so timing is critical
* Develop an efficient personal method to plan a navigation flight at home by **continued practice**
* Use the ERSA to practice a circuit join at an unfamiliar aerodrome in your head *(further covered in your training and CASA AC 91-10)*
* Ask your instructor questions! There is a **lot more** to air navigation.

---

## RPC Navigation Endorsement

###### Tips

* Use Google Earth as part of your preparation!
* What will Heck Field look like from a few miles out?

<p style="text-align: center">
  <a href="images/yhec-google-earth.png">
    <img src="images/yhec-google-earth.png" alt="YHEC" width="800px" style="border: 4px solid #555"/>
  </a>
</p>

---

## RPC Navigation Endorsement

###### You first Navigation training exercise

<p style="text-align: center; font-size: x-large">
  Your choice!
</p>

* **YRED Redcliffe**

  missed approach, then fly back home Archerfield
* **YHEC Jacobs Well Heckfield**

  land, grab a coffee, then fly back home Archerfield
* **YKBN Kooralbyn**

  land, grab a coffee, then fly back home Archerfield

## RPC Navigation Endorsement { data-transition="concave-out" }

###### What's next?

* After RPC with Navigation Endorsement and PAX Carrying Endorsement&hellip;
  * RPL conversion in heavier aircraft
  * Learn other aircraft systems, some more complex systems
  * *That's the easy bit!*
  * Initial RPC and navigation training is the harder bit
* PPL written exam
* Improve flying standard
* PPL Flight Test

<p style="text-align: center">
  <a href="images/rpc-whats-next.png">
    <img src="images/rpc-whats-next.png" alt="RPC What's Next" width="800px" style="border: 4px solid #555"/>
  </a>
</p>

---

