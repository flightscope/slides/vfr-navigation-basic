## Fuel Planning { data-transition="convex-in" }

* We have not covered Fuel Planning in this briefing
* Fuel Planning is covered in more detail during navigation training
* In your handout, you will find:
  * A fuel plan grid, ready to be filled out
  * The definitions verbatim from CASA Advisory Circular 91-15
* Fuel planning requirements were updated 02 December 2021

---

## Fuel Planning

* However, let's cover a little about minimum fuel reserve, since it applies to all your flights
* All day VFR flights have a minimum fuel reserve
* Under CASA AC 91-15, that minimum reserve is at least **30 minutes** of fuel
* Under some circumstances for navigation, you may be required to carry even more reserve fuel *(this is covered further in training)*
* The Eurofox burns 20 Litres per hour, so minimum fuel onboard at all times is 10 Litres
* The most common cause of engine failure is **fuel exhaustion**

---

## Fuel Planning

* What actions will we take if we make a mistake and inadvertently burn into our fuel reserve?
* Or, we haven't yet started into our fuel reserve, but we will by the time we get to a suitable airport?
* We have an <span style="color: red; font-weight: bold">Emergency Fuel Situation</span>

---

## Fuel Planning

###### Emergency Fuel Situation

<p style="text-align: center">
  <a href="images/emergency-fuel.png">
    <img src="images/emergency-fuel.png" alt="Emergency Fuel" width="800px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Fuel Planning

###### Emergency Fuel Situation { data-transition="concave-out" }

<p style="text-align: center">
  <a href="images/emergency-fuel-annotated.png">
    <img src="images/emergency-fuel-annotated.png" alt="Emergency Fuel (annotated)" width="800px" style="border: 4px solid #555"/>
  </a>
</p>
