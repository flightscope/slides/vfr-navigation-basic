## Dead Reckoning { data-transition="convex-in" }

<p style="text-align: center; font-size: x-large">
  What is my current position and what time will I arrive at a future position?
</p>

---

## Dead Reckoning

* With reference to the VTC
  * I know I was at TARGET at 1230 *(previous position fix)*
  * I am now at MOUNT COTTON at 1233 *(current position fix)*
  * **What time will I arrive at the North Stradbroke east coast *(via MT WILLES)*?**
  * **What is my ground speed?**

---

## Dead Reckoning

* TARGET to MOUNT COTTON is approximately 6 NM
* MOUNT COTTON to the North Stradbroke east coast is approximately 12 NM
* TARGET to MOUNT COTTON: 3 minutes
* Time of arrival overhead MOUNT COTTON: **1233**
* Estimated time of arrival overhead North Stradbroke east coast: **1239**

---

## Dead Reckoning { data-transition="concave-out" }

* I flew TARGET to MOUNT COTTON, or 6 nautical miles, in 3 minutes
* That means I would fly 120 nautical miles in 60 minutes
* 120 nautical miles per hour
* My ground speed is **120 knots**

<p style="text-align: center">
  <a href="tommy-foxbat.jpg">
    <img src="images/tommy-foxbat.jpg" alt="Tommy lovin the foxbat" width="480px" style="border: 4px solid #555"/>
  </a>
</p>

---

