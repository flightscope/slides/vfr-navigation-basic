## Long Final RWY14 Gold Coast { data-transition="convex-in" }

<p style="text-align: center">
  <a href="images/ybcg-final-3.jpg">
    <img src="images/ybcg-final-3.jpg" alt="Long Final RWY14 Gold Coast" width="420px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Short Final RWY14 Gold Coast { data-transition="convex-in" }

<p style="text-align: center">
  <a href="images/ybcg-final-2.jpg">
    <img src="images/ybcg-final-2.jpg" alt="Short Final RWY14 Gold Coast" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Short Final RWY32 Gold Coast { data-transition="convex-in convex-out" }

<p style="text-align: center">
  <a href="images/ybcg-final-1.jpg">
    <img src="images/ybcg-final-1.jpg" alt="Short Final RWY32 Gold Coast" width="750px" style="border: 4px solid #555"/>
  </a>
</p>

---

